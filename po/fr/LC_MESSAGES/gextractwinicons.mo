��          �   %   �      P  *   Q     |  |   �  	             $     1     E     M     c     q     x     �  4   �     �  
   �     �     �     	                          (     -  �  3  7   �      �  �        �     �     �     �     �     �            $   '     L  ?   P     �     �     �  /   �  	   �     �             	             "                                                                  	                
                                           %d resources found (%d resources selected) <b>Available resources:</b> A GTK utility to extract cursors, icons and png image previews from MS Windows resource files (like .exe, .dll, .ocx, .cpl). All files Bits Deselect all Destination folder: Extract Extraction completed. File to open: Height MS Windows compatible files Name Please select a file to list the available resources Preview Select all Select only PNGs Select path for saving Size Type Width cursor cursors icon icons Project-Id-Version: gextractwinicons 0.3
Report-Msgid-Bugs-To: Fabio Castelli <muflone@vbsimple.net>
POT-Creation-Date: 2010-01-03 16:41+0100
PO-Revision-Date: 2010-01-03 16:44+0100
Last-Translator: Emmanuel <emmanuel@simple-linux.com>
Language-Team: French
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 %d ressources trouvées (%d ressources sélectionnées) <b>Ressources disponibles : </b> Un outil GTK pour extraire pointeurs, icônes et aperçus des images png des fichiers de ressources pour MS Windows (comme .exe, .dll, .ocx, .cpl). Tous les fichiers Bits Tous désélectionner Dossier de destination: Extraire Extraction terminée. Fichier à ouvrir : Hauteur Fichiers compatibles avec MS Windows Nom Sélectionner un fichier pour lister les ressources disponibles Aperçu Tous sélectionner Sélectionner que PNG Sélectionner le parcours pour l'enregistrement Dimension Type Largeur pointeur pointeurs icône icônes 