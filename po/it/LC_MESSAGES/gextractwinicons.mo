��          �   %   �      P  *   Q     |  |   �  	             $     1     E     M     c     q     x     �  4   �     �  
   �     �     �     	                          (     -  �  3  +   �     �  �        �     �     �     �     �     �     �          	     &  5   +  	   a     k     {  (   �  
   �     �  	   �     �     �     �     �                                                                  	                
                                           %d resources found (%d resources selected) <b>Available resources:</b> A GTK utility to extract cursors, icons and png image previews from MS Windows resource files (like .exe, .dll, .ocx, .cpl). All files Bits Deselect all Destination folder: Extract Extraction completed. File to open: Height MS Windows compatible files Name Please select a file to list the available resources Preview Select all Select only PNGs Select path for saving Size Type Width cursor cursors icon icons Project-Id-Version: gextractwinicons 0.3
Report-Msgid-Bugs-To: Fabio Castelli <muflone@vbsimple.net>
POT-Creation-Date: 2010-01-03 16:41+0100
PO-Revision-Date: 2010-01-03 16:44+0100
Last-Translator: Fabio Castelli <muflone@vbsimple.net>
Language-Team: Italian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %d risorse trovate (%d risorse selezionate) <b>Risorse disponibili:</b> Un'utilità GTK per estrarre cursori, icone e anteprime delle immagini png dai files di risorse per MS Windows (come .exe, .dll, .ocx, .cpl). Tutti i files Bit Deseleziona tutto Cartella di destinazione: Estrarre Estrazione completata. File da aprire: Altezza Files compatibili MS Windows Nome Scegliere un file per elencare le risorse disponibili Anteprima Seleziona tutto Seleziona solo PNG Seleziona il percorso per il salvataggio Dimensione Tipo Larghezza cursore cursori icona icone 